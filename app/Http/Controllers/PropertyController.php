<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use File;
use Storage;
use Image;

class PropertyController extends Controller
{
    public $successStatus = 200;
	
	public function addProperty(Request $request)
    {
        $data = array(
            'user_id' => $request['user_id'],
            'title' => $request['title'],
            'description' => $request['description'],
            'property_purpose' => $request['property_purpose'],
            'property_type' => $request['property_type'],
            'country' => $request['country'],
            'state' => $request['state'],
            'city' => $request['city'],
            'town' => $request['town'],
            'address' => $request['address'],
            'phone' => $request['phone'],
            'price' => $request['price'],
            'land_area' => $request['land_area'],
            'property_unit' => $request['property_unit']
        );
        $inserted_property_id = DB::table('properties')->insertGetId($data);
        // $property_details = array();
        // if($inserted_property_id){
        //     $property_details['property_data'] = \DB::table('properties')->where('id', $inserted_property_id)->first();
        // }
        
        if (!empty($request->files) && $request->hasFile('property_image')) {
            $file = $request->file('property_image');
            $type = $file->getClientOriginalExtension();
            if ($type == 'jpg' or $type == 'PNG' or $type == 'png' or $type == 'jpeg') {
                $file_temp_name = 'property-' . $inserted_property_id . '-' . time() . '.' . $type;
                $upload_path = storage_path() . '/uploads/property/' . $inserted_property_id;
                $url_path = url('storage/uploads/property/') . $inserted_property_id;
                $file->move($upload_path, $file_temp_name);
                $img_data = array(
                    'property_id'=> $inserted_property_id,
                    'property_image' => $file_temp_name
                );
                $prop_img = DB::table('property_images')->insert($img_data);
            }
        }
        // if($prop_img){
        //     $property_images = \DB::table('property_images')->where('property_id', $inserted_property_id)->get();
        //     if(isset($property_images) && count($property_images) > 0){
        //         foreach($property_images as $prop_imgs){
        //             $property_details['path'] = $url_path.'/'.$prop_imgs->property_image;
        //         }
        //     }
        // }
        $property_details =  \DB::table('properties')
                ->selectRaw('CONCAT("' . url('/storage/uploads/property/') . '/'.$inserted_property_id.'/", '.$inserted_property_id.', "/" ,GROUP_CONCAT(property_images.property_image)) as property_images, properties.*')
                ->leftJoin('property_images', 'property_images.property_id', '=', $inserted_property_id)
                ->groupBy('properties.id')
                ->get();

        if ($prop_img || $inserted_property_id)
            return response()->json([
                'success' => true,
                'message' => 'Property added successfully',
                'data' => $property_details,
            ]);
        else
            return response()->json([
                'success' => false,
                'message' => 'Property could not be added'
            ], 500);
    }

    public function updateProperty(Request $request){
        if(isset($request['id']) && !empty($request['id']) && $request['id'] != 0){
            $data = array();
            if(isset($request['title']) && !empty($request['title'])){
                $data['title'] = $request['title'];
            }
            if(isset($request['description']) && !empty($request['description'])){
                $data['description'] = $request['description'];
            }
            if(isset($request['property_purpose']) && !empty($request['property_purpose'])){
                $data['property_purpose'] = $request['property_purpose'];
            }
            if(isset($request['property_type']) && !empty($request['property_type'])){
                $data['property_type'] = $request['property_type'];
            }
            if(isset($request['country']) && !empty($request['country'])){
                $data['country'] = $request['country'];
            }
            if(isset($request['state']) && !empty($request['state'])){
                $data['state'] = $request['state'];
            }
            if(isset($request['city']) && !empty($request['city'])){
                $data['city'] = $request['city'];
            }
            if(isset($request['town']) && !empty($request['town'])){
                $data['town'] = $request['town'];
            }
            if(isset($request['address']) && !empty($request['address'])){
                $data['address'] = $request['address'];
            }
            if(isset($request['phone']) && !empty($request['phone'])){
                $data['phone'] = $request['phone'];
            }
            if(isset($request['price']) && !empty($request['price'])){
                $data['price'] = $request['price'];
            }
            if(isset($request['land_area']) && !empty($request['land_area'])){
                $data['land_area'] = $request['land_area'];
            }
            if(isset($request['property_unit']) && !empty($request['property_unit'])){
                $data['property_unit'] = $request['property_unit'];
            }
            $is_update = \DB::table('properties')->where('id', $request['id'])->update($data);
            // if($is_update){
            //     $property_details['property_data'] = \DB::table('properties')->where('id', $request['id'])->first();
            // }            
            if (!empty($request->files) && $request->hasFile('property_image')) {
                $file = $request->file('property_image');
                $type = $file->getClientOriginalExtension();
                if ($type == 'jpg' or $type == 'PNG' or $type == 'png' or $type == 'jpeg') {
                    $file_temp_name = 'property-' . $request['id'] . '-' . time() . '.' . $type;
                    $upload_path = storage_path() . '/uploads/property/' . $request['id'];
					$url_path = url('storage/uploads/property/') . $request['id'];
                    $file->move($upload_path, $file_temp_name);
                    $img_data = array(
                        'property_id'=> $request['id'],
                        'property_image' => $file_temp_name
                    );
                    $prop_img = DB::table('property_images')->where('property_id', $request['id'])->update($img_data);
                } else {
                    $success['status'] = 0;
                    $success['message'] = trans('api.File type should be .PNG, .JPG or .JPEG');
                    return response()->json($success, $this->successStatus, ['Content-Type' => 'application/json'], JSON_NUMERIC_CHECK);
                }
            }
            $property_details =  \DB::table('properties')
                ->selectRaw('CONCAT("' . url('/storage/uploads/property/') . '/'.$request['id'].'/", '.$request['id'].', "/" ,GROUP_CONCAT(property_images.property_image)) as property_images, properties.*')
                ->leftJoin('property_images', 'property_images.property_id', '=', $request['id'])
                ->groupBy('properties.id')
                ->get();

            // if($prop_img){
            //     $property_images = \DB::table('property_images')->where('property_id', $request['id'])->get();
            //     if(isset($property_images) && count($property_images) > 0){
            //         foreach($property_images as $prop_imgs){
            //             $property_details['path'] = $url_path.'/'.$prop_imgs->property_image;
            //         }
            //     }
            // }
            if ($prop_img || $is_update)
                return response()->json([
                    'success' => true,
                    'message' => 'Property updated successfully',
                    'data' => $property_details,
                ]);
            else
                return response()->json([
                    'success' => false,
                    'message' => 'Property could not be updated'
                ], 500);

        } 
    }
	
	public function searchProperty(Request $request){
        if(count($request->all()) > 0){
            $properties =  \DB::table('properties')
                ->selectRaw('CONCAT("' . url('/storage/uploads/property/') . '/", properties.id, "/" ,GROUP_CONCAT(property_images.property_image)) as property_images, properties.*')
                ->leftJoin('property_images', 'property_images.property_id', '=', 'properties.id')
                ->groupBy('properties.id')
                ->get();
            return response()->json([
                'success' => true,
                'data' => $properties
            ], 400);
        }else{
            return response()->json([
                'success' => false,
                'message' => 'No data available'
            ], 500);
        }
	}
}
