<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;
class UserController extends Controller
{
	public $successStatus = 200;
	
    public function updateUser(Request $request){
		if(isset($request['id']) && $request['id'] != '' && $request['id'] != 0){
			$data = array();
			if(isset($request['name']) && !empty($request['name'])){
				$data['name'] = $request['name'];
			}
			if(isset($request['phone']) && !empty($request['phone'])){
				$data['phone'] = $request['phone'];
			}
			if(isset($request['address']) && !empty($request['address'])){
				$data['address'] = $request['address'];
			}
			if(isset($request['country']) && !empty($request['country'])){
				$data['country'] = $request['country'];
			}
			if(isset($request['state']) && !empty($request['state'])){
				$data['state'] = $request['state'];
			}
			if(isset($request['city']) && !empty($request['city'])){
				$data['city'] = $request['city'];
			}
			if (!empty($request->files) && $request->hasFile('profile_image')) {
            $file = $request->file('profile_image');
            $type = $file->getClientOriginalExtension();
				if ($type == 'jpg' or $type == 'PNG' or $type == 'png' or $type == 'jpeg') {
					$file_temp_name = 'profie-' . $request['id'] . '-' . time() . '.' . $type;
					$upload_path = storage_path() . '/uploads/users/' . $request['id'];
					$url_path = url('storage/uploads/users/') . $request['id'];
					
					$file->move($upload_path, $file_temp_name);
					$data['profile_image'] = $file_temp_name;
				}
			}
			$res = \DB::table('users')->where('id',$request['id'])->update($data);
			$user_info = array();
			$user_info['user'] = \DB::table('users')->where('id', $request['id'])->first();
			$user_info['image_path'] = $url_path.'/'.$file_temp_name;
			if($res){
				$success['status'] = 0;
				$success['message'] = trans('User Successfully updated');
				$success['user_info'] = $user_info;
				return response()->json($success, $this->successStatus, ['Content-Type' => 'application/json'], JSON_NUMERIC_CHECK);
			}else{
				$success['status'] = 0;
				$success['message'] = trans('api.File type should be .PNG, .JPG or .JPEG');
				return response()->json($success, $this->successStatus, ['Content-Type' => 'application/json'], JSON_NUMERIC_CHECK);
			}
		}
	}
	
	public function delUser(Request $request){
		if(isset($request['id']) && $request['id'] != 0 && $request['id'] != ''){
			$data = array('status' => 0);
			$res = \DB::table('users')->where('id',$request['id'])->update($data);
			if($res){
				$success['status'] = 0;
				$success['message'] = trans('api.user deleted successfully');
				return response()->json($success, $this->successStatus, ['Content-Type' => 'application/json'], JSON_NUMERIC_CHECK);
			}else{
				$success['status'] = 0;
				$success['message'] = trans('api.user does not exist');
				return response()->json($success, $this->successStatus, ['Content-Type' => 'application/json'], JSON_NUMERIC_CHECK);
			}
		}
	}
}
