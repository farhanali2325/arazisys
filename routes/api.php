<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'prefix' => 'auth'
], function () {
    Route::post('login', 'AuthController@login');
    Route::post('signup', 'AuthController@signup');
	Route::post('logout', 'AuthController@logout');
  
    Route::group([
      'middleware' => 'auth:api'
    ], function() {
        Route::get('user', 'AuthController@user');
    });
	Route::post('update-user', 'UserController@updateUser');
	Route::post('delete-user', 'UserController@delUser');
    Route::post('search-property', 'PropertyController@searchProperty');
    Route::post('add-property', 'PropertyController@addProperty');
    Route::post('update-property', 'PropertyController@updateProperty');
    
});



Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
